<div class="page-title">
  <div class="title_left">
    <h3>Member</h3>
  </div>
</div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Member </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <?php foreach($data as $d) { ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/User/editMember/<?php echo $d['id_member'] ?>" method="POST">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama User <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nama" name="nama" required="required" value="<?php echo $d['nama'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="username" name="username" required="required" value="<?php echo $d['username'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="password" name="password" required="required" value="<?php echo $d['password'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email" name="email" required="required" value="<?php echo $d['email'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notelp">Nomor Telepon <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="notelp" name="notelp" required="required" value="<?php echo $d['notelp'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fakultas">Fakultas <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="fakultas">
                              <option>Pilih Fakultas</option>
                              <?php foreach($fakultas as $f) { 
                                if($f['id_fakultas'] == $d['id_fakultas']) { ?>
                                <option value="<?php echo $f['id_fakultas'] ?>" selected><?php echo $f['nama_fakultas'] ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $f['id_fakultas'] ?>"><?php echo $f['nama_fakultas'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jurusan">Jurusan <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="jurusan">
                              <option>Pilih Jurusan</option>
                              <?php foreach($jurusan as $j) { 
                                if($j['id_jurusan'] == $d['id_jurusan']) { ?>
                                <option value="<?php echo $j['id_jurusan'] ?>" selected><?php echo $j['nama_jurusan'] ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $j['id_jurusan'] ?>"><?php echo $j['nama_jurusan'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>
                    </form>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('select[name="fakultas"]').on('change', function() {
            var fakultasID = $(this).val();
            if(fakultasID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/User/jurusanAjax/',
                    type: "POST",
                    data: { id: fakultasID},
                    dataType: "json",
                    success:function(data) {
                        $('select[name="jurusan"]').empty();
                        $('select[name="jurusan"]').append('<option>Pilih Jurusan</option>');
                        $.each(data, function(key, value) {
                            $('select[name="jurusan"]').append('<option value="'+ value.id_jurusan +'">'+ value.nama_jurusan +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="jurusan"]').empty();
            }
        });
    });
</script>