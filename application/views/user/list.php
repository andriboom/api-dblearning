<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>User</h3>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar User</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="tab">
        <button type="button" class="tablinks active" onclick="openCity(event, 'Member')">Member</button>
        <button type="button" class="tablinks" onclick="openCity(event, 'Kontributor')">Kontributor</button>
      </div>

      <div id="Member" class="tabcontent" style="display: block;">
        <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Username</th>
              <th>Password</th>
              <th>Fakultas</th>
              <th>Jurusan</th>
              <th>Tindakan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($member as $m) { ?>
            <tr>
              <td><?php echo $no ?></td>
              <td><?php echo $m['nama'] ?></td>
              <td><?php echo $m['email'] ?></td>
              <td><?php echo $m['username'] ?></td>
              <td><?php echo $m['password'] ?></td>
              <td><?php echo $m['nama_fakultas'] ?></td>
              <td><?php echo $m['nama_jurusan'] ?></td>
              <td><center><a href="<?php echo base_url() ?>index.php/User/formEditMember/<?php echo $m['id_member'] ?>" title="Edit User"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/User/hapusMember/<?php echo $m['id_member'] ?>" title="Hapus User"><p class="fa fa-trash fa-lg"></p></a></center></td>
            </tr>
            <?php $no++;} ?>
          </tbody>
        </table>    
      </div>

      <div id="Kontributor" class="tabcontent">
        <table id="datatable" class="table table-striped dt-responsive" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Password</th>
              <th>Mata Pelajaran</th>
              <th>Tindakan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($kontributor as $k) { ?>
            <tr>
              <td><?php echo $no ?></td>
              <td><?php echo $k['nama'] ?></td>
              <td><?php echo $k['username'] ?></td>
              <td><?php echo $k['password'] ?></td>
              <td><?php echo $k['nama_jurusan'] ?></td>
              <td><center><a href="<?php echo base_url() ?>index.php/User/formEditKontributor/<?php echo $k['id_kontributor'] ?>" title="Edit User"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/User/hapusKontributor/<?php echo $k['id_kontributor'] ?>" title="Hapus User"><p class="fa fa-trash fa-lg"></p></a></center></td>
            </tr>
            <?php $no++;} ?>
          </tbody>
        </table> 
      </div>
    </div>
  </div>
</div>
<script>
    function openCity(evt, command) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(command).style.display = "block";
        evt.currentTarget.className += " active";
    }
    </script>