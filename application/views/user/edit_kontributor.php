<div class="page-title">
  <div class="title_left">
    <h3>Kontributor</h3>
  </div>
</div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Kontributor </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <?php foreach($data as $d) { ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/User/editKontributor/<?php echo $d['id_kontributor'] ?>" method="POST">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama User <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nama" name="nama" required="required" value="<?php echo $d['nama'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="username" name="username" required="required" value="<?php echo $d['username'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="password" name="password" required="required" value="<?php echo $d['password'] ?>" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universitas">Mata Pelajaran <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="mapel">
                              <option>Pilih Mata Pelajaran</option>
                              <?php foreach($mapel as $m) { 
                                if($m['id_paket'] == $d['mapel']) { ?>
                                <option value="<?php echo $m['id_paket'] ?>" selected><?php echo $m['nama_paket'] ?></option>
                              <?php } else { ?>
                                <option value="<?php echo $m['id_paket'] ?>"><?php echo $m['nama_paket'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>
                    </form>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>