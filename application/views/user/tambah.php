<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>User</h3>
  </div>
</div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah User </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <div class="tab">
                      <button type="button" class="tablinks active" onclick="openCity(event, 'Member')">Member</button>
                      <button type="button" class="tablinks" onclick="openCity(event, 'Kontributor')">Kontributor</button>
                    </div>

                    <div id="Member" class="tabcontent" style="display: block;">
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/User/tambahMember" method="POST">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama User <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notelp">Nomor Telepon <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="notelp" name="notelp" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fakultas">Fakultas <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="fakultas" id="fakultas">
                              <option>Pilih Fakultas</option>
                              <?php foreach($fakultas as $u) { ?>
                              <option value="<?php echo $u['id_fakultas'] ?>"><?php echo $u['nama_fakultas'] ?></option>
                              <?php } ?>
                            </select>
                            <div id="fakul">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jurusan">Jurusan <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="jurusan" id="jurusan">
                              <option>Pilih Jurusan</option>
                            </select>
                            <div id="juru">
                            </div>
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>

                    <div id="Kontributor" class="tabcontent">
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/User/tambahKontributor" method="POST">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama User <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universitas">Jurusan <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="mapel">
                              <option>Pilih Mata Pelajaran</option>
                              <?php foreach($jurusan as $m) { ?>
                              <option value="<?php echo $m['id_jurusan'] ?>"><?php echo $m['nama_jurusan'] ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('select[name="fakultas"]').on('change', function() {
            var fakultasID = $(this).val();
            if(fakultasID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/User/jurusanAjax/',
                    type: "POST",
                    data: { id: fakultasID},
                    dataType: "json",
                    success:function(data) {
                        $('select[name="jurusan"]').empty();
                        $('select[name="jurusan"]').append('<option>Pilih Jurusan</option>');
                        $.each(data, function(key, value) {
                            $('select[name="jurusan"]').append('<option value="'+ value.id_jurusan +'">'+ value.nama_jurusan +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="jurusan"]').empty();
            }
        });
    });

    $(document).on('change', '#lain', function(){
        if($(this).prop('checked')){
            $('#fakultas').attr('disabled', 'disabled');
            $('#jurusan').attr('disabled', 'disabled');
            document.getElementById('fakul').innerHTML = '<input type="text" placeholder="Masukkan Fakultas Lain" name="fakultas" class="form-control col-md-7 col-xs-12" />';
            document.getElementById('juru').innerHTML = '<input type="text" placeholder="Masukkan Jurusan Lain" name="jurusan" class="form-control col-md-7 col-xs-12" />';
        } else {
            $('#fakultas').removeAttr('disabled');
            $('#jurusan').removeAttr('disabled');
            document.getElementById('fakul').innerHTML = '';
            document.getElementById('juru').innerHTML = '';
        }
    });

    function openCity(evt, command) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(command).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>