<div class="page-title">
  <div class="title_left">
    <h3>Soal</h3>
  </div>

</div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Soal </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/Soal/tambahSoal" method="POST">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="judul_berita">Soal <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control col-md-7 col-xs-12" name="soal"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">Jumlah Pilihan<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" class="form-control col-md-7 col-xs-12" name="jml_pilihan" id="jml_pilihan">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Pilihan Jawaban <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div id="pilihan"></div>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
            <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
            <script type="text/javascript">
            $(document).on('change', '#jml_pilihan', function(){
                var jml = $(this).val();
                var element = '';
                var pil = '';
                console.log(jml);
                  for (var i = 1; i <= jml; i++) {
                    element += '<div><input type="text" name="pili'+i+'" value="'+i+'" readonly style="width: 20px"> <input type="radio" name="pil_jaw" value="'+i+'"> jawaban benar <input type="text" name="jawaban'+i+'" class="form-control col-md-7 col-xs-12"/></div>';
                  };
                  document.getElementById('pilihan').innerHTML = element;
              });
            </script>