<div class="page-title">
  <div class="title_left">
    <h3>Soal</h3>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Pilihan Jawaban</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Pilihan Jawaban</th>
            <th>Jawaban</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($pilihan as $data) { ?>
          <tr>
            <td><?php echo $data['pil_jawaban'] ?></td>
            <td><?php echo $data['jawaban'] ?></td>
            <td><center><a href="<?php echo base_url() ?>index.php/Soal/formEditPilihan/<?php echo $data['id_pilihan'] ?>" title="Edit Pilihan"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/Soal/hapusPilihan/<?php echo $data['id_pilihan'] ?>" title="Hapus Pilihan"><p class="fa fa-trash fa-lg"></p></a></center></td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>