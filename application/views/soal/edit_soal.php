<div class="page-title">
  <div class="title_left">
    <h3>Soal</h3>
  </div>

</div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Soal </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <?php foreach($soal as $d) { ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/Soal/editSoal/<?php echo $d['id_soal'] ?>" method="POST">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="judul_berita">Soal <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control col-md-7 col-xs-12" name="soal"><?php echo $d['soal'] ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">Jawaban Benar<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control col-md-7 col-xs-12" name="jwbn_benar">
                            <?php foreach($pilihan as $pil) { 
                              if($pil['pil_jawaban'] == $d['jawaban_benar']) { ?>
                              <option value="<?php echo $pil['pil_jawaban'] ?>" selected><?php echo $pil['jawaban'] ?></option>
                            <?php } else { ?>
                              <option value="<?php echo $pil['pil_jawaban'] ?>"><?php echo $pil['jawaban'] ?></option>
                            <?php }} ?>
                          </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>