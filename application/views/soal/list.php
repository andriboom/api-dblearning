<div class="page-title">
  <div class="title_left">
    <h3>Soal</h3>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Soal</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Soal</th>
            <th>Jawaban Benar</th>
            <th>Pilihan Jawaban</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($soal as $data) { ?>
          <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $data['soal'] ?></td>
            <td><?php echo $data['jawaban'] ?></td>
            <td><a href="<?php echo  base_url() ?>index.php/Soal/pilihanJawaban/<?php echo $data['id_soal'] ?>"><button class="btn btn-success">Lihat Pilihan Jawaban</button></a></td>
            <td><center><a href="<?php echo base_url() ?>index.php/Soal/formEditSoal/<?php echo $data['id_soal'] ?>" title="Edit Soal"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/Soal/hapusSoal/<?php echo $data['id_soal'] ?>" title="Hapus Soal"><p class="fa fa-trash fa-lg"></p></a></center></td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>