<div class="page-title">
  <div class="title_left">
    <h3>Berita</h3>
  </div>

</div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Berita </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <?php foreach($data as $d) { ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/Berita/editBerita/<?php echo $d['id_berita'] ?>" method="POST">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="judul_berita">Judul Berita <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="judul_berita" name="judul_berita" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $d['judul'] ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">Berita <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo $this->ckeditor->editor("berita",$d['berita']); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Status <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php if($d['status'] == 'show') { ?>
                          Show: <input type="radio" class="flat" checked="" name="status" id="show" value="show" checked="" required /> Hide:
                        <input type="radio" class="flat" name="status" id="hide" value="hide" />
                          <?php } else { ?>
                          Show: <input type="radio" class="flat" name="status" id="show" value="show" checked="" required /> Hide:
                        <input type="radio" class="flat" checked="" name="status" id="hide" value="hide" />
                          <?php } ?>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>