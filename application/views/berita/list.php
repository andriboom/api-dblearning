<div class="page-title">
  <div class="title_left">
    <h3>Berita</h3>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Berita</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Berita</th>
            <th>Status</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($berita as $data) { ?>
          <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $data['judul'] ?></td>
            <td><?php echo $data['berita'] ?></td>
            <td><?php echo $data['status'] ?></td>
            <td><center><a href="<?php echo base_url() ?>index.php/Berita/formEdit/<?php echo $data['id_berita'] ?>" title="Edit User"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/Berita/hapusBerita/<?php echo $data['id_berita'] ?>" title="Hapus User"><p class="fa fa-trash fa-lg"></p></a></center></td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>