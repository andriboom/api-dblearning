<style type="text/css">
        .modal{
          position: fixed;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          overflow: auto;
          background-color: rgb(0,0,0);
          background-color: rgba(0,0,0,0.4);
        }
        .modal-content{
          background-color: #fefefe;
          text-align: center;
          margin: 15% auto;
          padding: 20px;
          border: 1px solid #888;
          width: 80%;
        }
      </style>
<div class="page-title">
  <div class="title_left">
    <h3>Pembayaran</h3>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Pembayaran</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama User</th>
            <th>Nama Paket</th>
            <th>Durasi</th>
            <th>Expiration</th>
            <th>Total Bayar</th>
            <th>Nomor Invoice</th>
            <th>Tipe Transfer</th>
            <th>Pemilik Bank</th>
            <th>Nama Bank</th>
            <th>Status Invoice</th>
            <th>Bukti Pembayaran</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($pembayaran as $data) { ?>
          <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $data['nama_user'] ?></td>
            <td><?php echo $data['nama_paket'] ?></td>
            <td><?php echo $data['durasi'] ?></td>
            <td><?php echo $data['expiration'] ?></td>
            <td><?php echo "Rp. ".number_format($data['total_bayar'],0,",",".") ?></td>
            <td><?php echo $data['no_invoice'] ?></td>
            <td>
              <?php if($data['tipe_transfer']=='1') {
                echo "Otomatis";
                } else {
                echo "Manual";
                  } ?>
            </td>
            <td><?php echo $data['pemilik_bank'] ?></td>
            <td><?php echo $data['nama_bank'] ?></td>
            <td>
              <?php if($data['status_invoice']=='1') {
                echo "Aktif";
                } else {
                  echo "Pending";
                  } ?>
            </td>
            <td><a href="#" data-toggle="modal" data-target="#bukti"><img src="<?php echo base_url().$data['file_gambar'] ?>" width="80px"></a></td>
            <td>
              <center>
                <?php if($data['status_invoice']!='1') { ?>
                <a href="<?php echo base_url() ?>index.php/Pembayaran/editStatus/<?php echo $data['id_invoice'] ?>" title="Verifikasi"><p class="fa fa-check fa-lg"></p></a>&nbsp;
                <?php } ?>
                <a href="<?php echo base_url() ?>index.php/Pembayaran/hapusPembayaran/<?php echo $data['id_invoice'] ?>" title="Hapus Invoice"><p class="fa fa-trash fa-lg"></p></a>
              </center>
            </td>
          </tr>
          <div id="bukti" class="modal" style="display: none; !important">
            <div class="modal-content">
              <h3>Bukti Transfer</h3>
              <hr></hr>
              <div class="clearfix"></div>
              <img src="<?php echo base_url().$data['file_gambar'] ?>" width="500px">
            </div>
          </div>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>