<div class="page-title">
  <div class="title_left">
    <h3>Video</h3>
  </div>

  <div class="title_right">
    <div class="form-group pull-right top_search">
      <div class="input-group">
        <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg" type="button">Tambah Video</button>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Video</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php echo validation_errors(); ?>
        <?php echo $this->session->flashdata('error'); ?>
      <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Video</th>
            <th>Nama Fakultas</th>
            <th>Nama Jurusan</th>
            <th>Nama Materi</th>
            <th>Nama Konten</th>
            <th>Nama Kontributor</th>
            <th>Status</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($video as $data) { ?>
          <tr>
            <td><?php echo $no ?></td>
            <td>
              <video width="300" controls>
                <source src="<?php echo base_url() ?>assets/video/<?php echo $data['video'] ?>" type="video/mp4">
                Your browser does not support HTML5 video.
              </video>
            </td>
            <td><?php echo $data['nama_fakultas'] ?></td>
            <td><?php echo $data['nama_jurusan'] ?></td>
            <td><?php echo $data['nama_materi'] ?></td>
            <td><?php echo $data['nama_konten'] ?></td>
            <td><?php echo $data['nama_kontributor'] ?></td>
            <td><?php if($data['status']==0) { echo 'FREE'; } else { echo 'PAID'; } ?></td>
            <td><center><a href="<?php echo base_url() ?>index.php/Video/formEditVideo/<?php echo $data['id_video'] ?>" title="Edit Video"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/Video/hapusVideo/<?php echo $data['id_video'] ?>" title="Hapus Video"><p class="fa fa-trash fa-lg"></p></a></center></td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/Video/tambahVideo" method="POST" enctype="multipart/form-data">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Video Baru</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="video">Video 
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" id="video" name="video" required="required" class="form-control col-md-7 col-xs-12">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">Nama Kontributor <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="pemateri">
              <option>Pilih Nama Kontributor</option>
              <?php foreach($pemateri as $pem) { ?>
              <option value="<?php echo $pem['id_user'] ?>"><?php echo $pem['nama'] ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Fakultas <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="fakultas">
              <option>Pilih Nama Fakultas</option>
              <?php foreach($fakultas as $fa) { ?>
              <option value="<?php echo $fa['id_fakultas'] ?>"><?php echo $fa['nama_fakultas'] ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Jurusan <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="jurusan">
              <option>Pilih Nama Jurusan</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Materi <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="materi">
              <option>Pilih Nama Materi</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Konten <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="konten">
              <option>Pilih Nama Konten</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Status Video <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="status">
              <option>Pilih Status Video</option>
              <option value="0">FREE</option>
              <option value="1">PAID</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
      </div>
    </div>
  </div>
  </form>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('select[name="fakultas"]').on('change', function() {
            var fakultasID = $(this).val();
            if(fakultasID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Video/jurusanAjax/'+fakultasID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="jurusan"]').empty();
                        $('select[name="jurusan"]').append('<option>Pilih Nama Jurusan</option>');
                        $.each(data, function(key, value) {
                            $('select[name="jurusan"]').append('<option value="'+ value.id_jurusan +'">'+ value.nama_jurusan +'</option>');
                            console.log(value.id_jurusan);
                        });
                    }
                });
            }else{
                $('select[name="jurusan"]').empty();
            }
        });

        $('select[name="jurusan"]').on('change', function() {
            var jurusanID = $(this).val();
            if(jurusanID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Video/materiAjax/'+jurusanID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="materi"]').empty();
                        $('select[name="materi"]').append('<option>Pilih Nama Materi</option>');
                        $.each(data, function(key, value) {
                            $('select[name="materi"]').append('<option value="'+ value.id_materi +'">'+ value.nama_materi +'</option>');
                            console.log(value.id_materi);
                        });
                    }
                });
            }else{
                $('select[name="materi"]').empty();
            }
        });

        $('select[name="materi"]').on('change', function() {
            var materiID = $(this).val();
            if(materiID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Video/kontenAjax/'+materiID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="konten"]').empty();
                        $('select[name="konten"]').append('<option>Pilih Nama Konten</option>');
                        $.each(data, function(key, value) {
                            $('select[name="konten"]').append('<option value="'+ value.id_konten +'">'+ value.nama_konten +'</option>');
                            console.log(value.id_konten);
                        });
                    }
                });
            }else{
                $('select[name="konten"]').empty();
            }
        });
    });
</script>