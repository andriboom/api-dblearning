<div class="page-title">
  <div class="title_left">
    <h3>Fakultas</h3>
  </div>

  <div class="title_right">
    <div class="form-group pull-right top_search">
      <div class="input-group">
        <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg" type="button">Tambah Paket Fakultas</button>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Fakultas</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php echo validation_errors(); ?>
        <?php echo $this->session->flashdata('error'); ?>
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Paket Fakultas</th>
            <th>Harga Paket Fakultas</th>
            <th>Jurusan</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($paket as $data) { ?>
          <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $data['nama_fakultas'] ?></td>
            <td><?php echo "Rp. ".number_format($data['harga'],0,",",".") ?></td>
            <td><a href="<?php echo base_url() ?>index.php/Video/listJurusan/<?php echo $data['id_fakultas'] ?>"><button class="btn btn-success" type="button">Lihat Jurusan</button></td>
            <td><center><a href="<?php echo base_url() ?>index.php/Video/formEditPaket/<?php echo $data['id_fakultas'] ?>" title="Edit Paket"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/Video/hapusPaket/<?php echo $data['id_fakultas'] ?>" title="Hapus Paket"><p class="fa fa-trash fa-lg"></p></a></center></td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/Video/tambahPaket" method="POST">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Paket Baru</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Paket Fakultas <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control col-md-7 col-xs-12" name="nama_paket">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Harga Paket Fakultas <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control col-md-7 col-xs-12" name="harga_paket">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
      </div>
    </div>
  </div>
  </form>
</div>