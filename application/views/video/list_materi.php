<div class="page-title">
  <div class="title_left">
    <h3>Materi</h3>
  </div>

  <div class="title_right">
    <div class="form-group pull-right top_search">
      <div class="input-group">
        <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg" type="button">Tambah Materi</button>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Materi</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php echo validation_errors(); ?>
        <?php echo $this->session->flashdata('error'); ?>
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Materi</th>
            <th>Konten</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($materi as $data) { ?>
          <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $data['nama_materi'] ?></td>
            <td><a href="<?php echo base_url() ?>index.php/Video/listKonten/<?php echo $this->uri->segment(3) ?>/<?php echo $data['id_jurusan'] ?>/<?php echo $data['id_materi'] ?>"><button class="btn btn-success" type="button">Lihat Konten</button></a></td>
            <td><center><a href="<?php echo base_url() ?>index.php/Video/formEditMateri/<?php echo $this->uri->segment(3) ?>/<?php echo $data['id_materi'] ?>" title="Edit Materi"><p class="fa fa-edit fa-lg"></p></a>&nbsp;<a href="<?php echo base_url() ?>index.php/Video/hapusMateri/<?php echo $this->uri->segment(3) ?>/<?php echo $data['id_jurusan'] ?>/<?php echo $data['id_materi'] ?>" title="Hapus Materi"><p class="fa fa-trash fa-lg"></p></a></center></td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/Video/tambahMateri/<?php echo $this->uri->segment(3) ?>/<?php echo $this->uri->segment(4) ?>" method="POST">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Materi Baru</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Materi <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control col-md-7 col-xs-12" name="nama_materi">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
      </div>
    </div>
  </div>
  </form>
</div>