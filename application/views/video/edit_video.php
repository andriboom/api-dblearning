<div class="page-title">
  <div class="title_left">
    <h3>Video</h3>
  </div>

</div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Video </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <?php foreach($data as $d) { ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo base_url() ?>index.php/Video/editVideo/<?php echo $d['id_video'] ?>" method="POST" enctype="multipart/form-data">

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="video">Video 
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="video" name="video" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">Nama Pemateri <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="pemateri">
                              <option>Pilih Nama Pemateri</option>
                              <?php foreach($pemateri as $pem) { 
                                if($pem['id_user'] == $d['id_user']) { ?>
                                  <option value="<?php echo $pem['id_user'] ?>" selected><?php echo $pem['nama'] ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $pem['id_user'] ?>"><?php echo $pem['nama'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Fakultas <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="fakultas">
                              <option>Pilih Nama Fakultas</option>
                              <?php foreach($fakultas as $pak) { 
                                if($pak['id_fakultas'] == $d['id_fakultas']) { ?>
                                  <option value="<?php echo $pak['id_fakultas'] ?>" selected><?php echo $pak['nama_fakultas'] ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $pak['id_fakultas'] ?>"><?php echo $pak['nama_fakultas'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Jurusan <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="jurusan">
                              <option>Pilih Nama Jurusan</option>
                              <?php foreach($jurusan as $jur) { 
                                if($jur['id_jurusan'] == $d['id_jurusan']) { ?>
                                  <option value="<?php echo $jur['id_jurusan'] ?>" selected><?php echo $jur['nama_jurusan'] ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $jur['id_jurusan'] ?>"><?php echo $jur['nama_jurusan'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Materi <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="materi">
                              <option>Pilih Nama Materi</option>
                              <?php foreach($materi as $mat) { 
                                if($mat['id_materi'] == $d['id_materi']) { ?>
                                  <option value="<?php echo $mat['id_materi'] ?>" selected><?php echo $mat['nama_materi'] ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $mat['id_materi'] ?>"><?php echo $mat['nama_materi'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Nama Konten <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="konten">
                              <option>Pilih Nama Konten</option>
                              <?php foreach($konten as $kon) { 
                                if($kon['id_konten'] == $d['id_konten']) { ?>
                                  <option value="<?php echo $kon['id_konten'] ?>" selected><?php echo $kon['nama_konten'] ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $kon['id_konten'] ?>"><?php echo $kon['nama_konten'] ?></option>
                              <?php }} ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Status Video <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" name="status">
                              <option>Pilih Status Video</option>
                              <?php if($d['status'] == 0) { ?>
                                  <option value="0" selected="">FREE</option>
                                  <option value="1">PAID</option>
                              <?php } else { ?>
                                  <option value="0">FREE</option>
                                  <option value="1" selected="">PAID</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <input type="submit" class="btn btn-success" name="submit" value="Submit">
                        </div>
                      </div>

                    </form>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('select[name="fakultas"]').on('change', function() {
            var fakultasID = $(this).val();
            if(fakultasID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Video/jurusanAjax/'+fakultasID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="jurusan"]').empty();
                        $('select[name="jurusan"]').append('<option>Pilih Nama Jurusan</option>');
                        $.each(data, function(key, value) {
                            $('select[name="jurusan"]').append('<option value="'+ value.id_jurusan +'">'+ value.nama_jurusan +'</option>');
                            console.log(value.id_jurusan);
                        });
                    }
                });
            }else{
                $('select[name="jurusan"]').empty();
            }
        });

        $('select[name="jurusan"]').on('change', function() {
            var jurusanID = $(this).val();
            if(jurusanID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Video/materiAjax/'+jurusanID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="materi"]').empty();
                        $('select[name="materi"]').append('<option>Pilih Nama Materi</option>');
                        $.each(data, function(key, value) {
                            $('select[name="materi"]').append('<option value="'+ value.id_materi +'">'+ value.nama_materi +'</option>');
                            console.log(value.id_materi);
                        });
                    }
                });
            }else{
                $('select[name="materi"]').empty();
            }
        });

        $('select[name="materi"]').on('change', function() {
            var materiID = $(this).val();
            if(materiID) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Video/kontenAjax/'+materiID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="konten"]').empty();
                        $('select[name="konten"]').append('<option>Pilih Nama Konten</option>');
                        $.each(data, function(key, value) {
                            $('select[name="konten"]').append('<option value="'+ value.id_konten +'">'+ value.nama_konten +'</option>');
                            console.log(value.id_konten);
                        });
                    }
                });
            }else{
                $('select[name="konten"]').empty();
            }
        });
    });
</script>