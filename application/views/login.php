<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
    <link href="<?php echo base_url('');?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('') ?>assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/style.css">

	</head>
	<body>
		<style type="text/css">
			body
			{
			    background-image: url("<?php echo base_url();?>assets/img/bg.jpg");
			    background-size: 100%;
			}
		</style>

		<div class="col-md-4 col-md-offset-4 form-login">
			<div class="outter-form-login">
				<!--  -->
				<!-- <h2 style="color: white">
					<center><b>SWCT</b></center>
				</h2> -->
				<center>
				<h1>MANAJEMEN DISPOSISI</h1><br>
					<img width="250px" height="250px" src="<?php echo base_url();?>assets/img/logo.png">
				</center>
				<?php echo form_open_multipart('login/cekLogin') ?>
					<h5 class="text-center title-login">
						<?php if (isset($sukses)) { ?>
						<p style="color: white;"><i><?php echo $sukses; ?>&nbsp</i></p>
						<?php } ?>
					</h5>
					<?php echo validation_errors(); ?>
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
							<input type="text" class="form-control" id="username" name="username" placeholder="Username">
						</div><br>
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input type="password" class="form-control" id="password" name="password" placeholder="Password">
						</div><br>
													
						<input type="submit" class="btn btn-block btn-group" value="Login" /><br><br>			
				<?php echo form_close(); ?>
				
	        </div>
	    </div>

	    <script src="<?php echo base_url('') ?>assets/js/jquery.min.js"></script>
	    <script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
	</body>
</html>