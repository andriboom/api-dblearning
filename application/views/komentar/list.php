<div class="page-title">
  <div class="title_left">
    <h3>Komentar</h3>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Komentar</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Konten</th>
            <th>Nama Kontributor</th>
            <th>Jumlah Komentar</th>
            <th>Komentar</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($komentar as $data) { ?>
          <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $data['nama_konten'] ?></td>
            <td><?php echo $data['nama_kontributor'] ?></td>
            <td><?php echo $data['jml'] ?></td>
            <td><a href="<?php echo base_url() ?>index.php/Komentar/listDetail/<?php echo $data['id_video'] ?>"><button class="btn btn-success">Lihat Komentar</button></a></td>
            <td><center><a href="<?php echo base_url() ?>index.php/Komentar/hapusKomentar/<?php echo $data['id_video'] ?>" title="Hapus User"><p class="fa fa-trash fa-lg"></p></a></center></td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>    
    </div>
  </div>
</div>