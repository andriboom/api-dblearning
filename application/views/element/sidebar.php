<!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Dashboard </a>
                  </li>
                  <li><a><i class="fa fa-user"></i> User <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>index.php/User/formTambah">Tambahkan User</a></li>
                      <li><a href="<?php echo base_url() ?>index.php/User">Daftar User</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Video <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>index.php/Video">Daftar Video</a></li>
                      <li><a href="<?php echo base_url() ?>index.php/Video/listPaket">Kategori</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-money"></i> Pembayaran <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>index.php/Pembayaran">Daftar Pembayaran</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-newspaper-o"></i> Berita <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>index.php/Berita/formTambah">Tambahkan Berita</a></li>
                      <li><a href="<?php echo base_url() ?>index.php/Berita">Daftar Berita</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-image"></i>Slider <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>index.php/Slider">Daftar Slider</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-comments"></i>Komentar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>index.php/Komentar">Daftar Komentar</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-file-o"></i> Soal Quiz <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>index.php/Soal/formTambah">Tambahkan Soal</a></li>
                      <li><a href="<?php echo base_url() ?>index.php/Soal">Daftar Soal</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->