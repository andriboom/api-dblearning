<div class="page-title">
  <div class="title_left">
    <h3>Slider</h3>
  </div>

  <div class="title_right">
    <div class="form-group pull-right top_search">
      <div class="input-group">
        <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm" type="button">Tambah Slider</button>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Slider</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <?php echo $this->session->flashdata('error'); ?>
      <div class="row">
        <?php foreach($slider as $data) { ?>
        <div class="col-md-4">
          <div class="thumbnail" style="height: 100% !important;">
            <div class="image view view-first">
              <img style="width: 100%; display: block;" src="<?php echo base_url() ?>assets<?php echo $data['file_slider'] ?>" alt="image" />
              <div class="mask" style="height: 100% !important;">
                <div class="tools tools-bottom" style="margin-top: 87px !important">
                  <a href="<?php echo base_url() ?>index.php/Slider/hapusSlider/<?php echo $data['id_slider'] ?>"><i class="fa fa-trash"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div> 
        <?php } ?> 
      </div> 
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/Slider/addSlider" method="POST" enctype="multipart/form-data">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel2">Upload Slider Baru</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
              <input type="file" id="slider" name="slider" required="required" class="form-control col-md-7 col-xs-12">
          </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
      </div>
    </div>
  </div>
  </form>
</div>