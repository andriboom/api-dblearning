<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('asia/jakarta');
	}


	function startQuiz($nama,$device_id)
	{
		//mengecek nama pada tabel user
		$this->db->where('nama', $nama);
		$checkUser = $this->db->get('tb_user')->row();
		if (empty($checkUser)) {
			//mengambil data setting
			$setting = $this->db->get('tb_setting_soal')->row();
			//apabila nama tidak ditemukan
			// user belum memulai sebelumnya
			$insert = $this->db->insert('tb_user', 
				array(
					'nama'=>$nama,
					'start_quiz'=>date("Y-m-d H:i:s"),
					'finish_quiz'=>date("Y-m-d H:i:s",strtotime("+".$setting->waktu_pengerjaan." hours")),
					'device_id'=>$device_id
				)
			);
			if ($insert) {
				$this->db->select('id_user,finish_quiz');
				$this->db->where('nama', $nama);
				$user = $this->db->get('tb_user')->row();
				//membuat data quiz
				$dataquiz = $this->membuatSoalQuiz($user->id_user);
				if (!empty($dataquiz)) {
					$output['status'] = true;
					$output['result'] = $dataquiz;
					$output['message'] = 'quiz dimulai batas waktu anda sampai '.$user->finish_quiz;
				}else{
					$output['status'] = false;
					$output['message'] = 'terjadi kesalahan mengambil data quiz';
				}			
			}else{
				$output['status'] = false;
				$output['message'] = 'gagal menyimpan data user';
			}
		}else{
			//apanila nama ditemukan, cek device id
			// user telah mmemulai sebelumnya
			// if ($device_id == $checkNama) {
			// 	// apabila sama
			// 	// db_get
			// }else{
			// 	// apabila tidak sama
			// 	$output['status'] = false;
			// 	$output['message'] = 'tidak di ijinkan mulai, lebih dari satu device';
			// }
			// sementara
				$output['status'] = false;
				$output['message'] = 'tidak di ijinkan mulai, lebih dari satu device';
		}
		return $output;
	}

	function membuatSoalQuiz($id_user)
	{
		//mengambil data setting
		$setting = $this->db->get('tb_setting_soal')->row();
		//mengambil data teori
		$this->db->where('kategori_soal', '1');
		$this->db->order_by('RAND ()');
		$result['teori'] = $this->db->get('tb_bank_soal',$setting->soal_teori)->result_array();

		//mengambil data praktikum
		$this->db->where('kategori_soal', '2');
		$this->db->order_by('RAND ()');
		$result['praktikum'] = $this->db->get('tb_bank_soal',$setting->soal_praktikum)->result_array();

		//menyimpan semua data quiz
		for ($i = 0; $i < count($result['teori']); $i++) {
			$this->db->insert('tb_jawaban_user', array(
				'user_id' => $id_user,
				'bank_soal_id'=>$result['teori'][$i]['id_bank_soal']
			));
			$this->db->where('bank_soal_id', $result['teori'][$i]['id_bank_soal']);
			$result['teori'][$i]['jawaban'] = $this->db->get('tb_bank_jawaban')->result();
		}
		for ($i = 0; $i < count($result['praktikum']); $i++) {
			$this->db->insert('tb_jawaban_user', array(
				'user_id' => $id_user,
				'bank_soal_id'=>$result['praktikum'][$i]['id_bank_soal']
			));
			$this->db->where('bank_soal_id', $result['praktikum'][$i]['id_bank_soal']);
			$result['praktikum'][$i]['jawaban'] = $this->db->get('tb_bank_jawaban')->row();
		}
		return $result;
	}



}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */
