<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('asia/jakarta');
	}


	function insertUser($data,$kodeunik,$token,$phone_id)
	{
		$this->db->where('email', $data['email']);
		$check = $this->db->get('member')->row();

		if (empty($check)) {
			$insert = $this->db->insert('member', $data);
			if ($insert) {
				$this->db->where('email', $data['email']);
				$member = $this->db->get('member')->row();
				
				$this->db->where('fcm_token', $token);
				$this->db->update('user', array('fcm_token' => "" ));

				$this->db->where('phone_id', $phone_id);
				$this->db->update('user', array('phone_id' => NULL));
				
				$user = array(
					'id_member'=>$member->id_member,
					'kode_unik'=>$kodeunik,
					'fcm_token'=>$token,
					'phone_id'=>$phone_id
				);
				$insertmember=$this->db->insert('user', $user);
				if ($insertmember) {
					$this->db->select('user.*,member.*,fakultas.*,premium_member.expiration');
					$this->db->where('member.email', $data['email']);
					$this->db->join('premium_member', 'premium_member.id_user = user.id_user','left');
					$this->db->join('member', 'member.id_member = user.id_member');
					$this->db->join('fakultas', 'member.id_fakultas = fakultas.id_fakultas');
					$output['result'] = $this->db->get('user')->row();
					$output['status'] = "success";
				}else{
					$output['status'] = "fail";
				}
			}else{
				$output['status'] = "fail";
			}
		}else{
			$output['status']= "exists";
		}
		return $output;
	}

	function forgotPassword($email)
	{
		$this->db->where('email', $email);
		$checkEmail = $this->db->get('member')->row();
		if (!empty($checkEmail)) {
			$this->db->where('email', $email);
			$this->db->where('expiration > ',date('Y-m-d H:i:s'));
			$this->db->order_by('expiration', 'desc');
			// $this->db->where('expiration',date('Y-m-d H:i:s'));
			// $this->db->or_where('status', '1');
			$checkExpiration = $this->db->get('forgot_password')->row();
			if (empty($checkExpiration)) {
				$batas = new DateTime(date('Y-m-d H:i:s'));
				$batas->modify('+30 minutes');
				$this->db->select('count(id_forgot)+1 as total');
				$getTotal = $this->db->get('forgot_password')->row();
				$data = array(
					'email' => $email,
					'code_verify' => md5($checkEmail->email.$getTotal->total),
					'expiration' => $batas->format('Y-m-d H:i:s'),
					'created_at' => date('Y-m-d H:i:s'));
				$insert = $this->db->insert('forgot_password', $data);
				if ($insert) {
					$output['code'] = md5($checkEmail->email.$getTotal->total);
					$output['status']="success";
				}else{
					$output['status']="notsend";				
				}
			}else{
				if ($checkExpiration->status=="0") {
					$output['status']="notexpiration";				
				}else{
					$batas = new DateTime(date('Y-m-d H:i:s'));
					$batas->modify('+30 minutes');
					$this->db->select('count(id_forgot)+1 as total');
					$getTotal = $this->db->get('forgot_password')->row();
					$data = array(
						'email' => $email,
						'code_verify' => md5($checkEmail->email.$getTotal->total),
						'expiration' => $batas->format('Y-m-d H:i:s'),
						'created_at' => date('Y-m-d H:i:s'));
					$insert = $this->db->insert('forgot_password', $data);
					if ($insert) {
						$output['code'] = md5($checkEmail->email.$getTotal->total);
						$output['status']="success";
					}else{
						$output['status']="notsend";				
					}
				}
				
			}
		}else{
			$output['status']="notexist";
		}
		return $output;
	}

	function getById($id){
		$this->db->select('user.*,member.*,fakultas.*,premium_member.expiration');
		$this->db->where('user.id_user', $id);
		$this->db->join('member', 'member.id_member = user.id_member');
		$this->db->join('fakultas', 'member.id_fakultas = fakultas.id_fakultas');
		$this->db->join('premium_member', 'premium_member.id_user = user.id_user','left');
		return $this->db->get('user')->row();
	}

	function login($email,$password,$token,$phone_id)
	{
		$this->db->select('user.*,member.*,fakultas.*,premium_member.expiration');
		// if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->db->where('email', $email);
		// }else{
		// 	$this->db->where('username', $email);
		// }
		// $this->db->where('phone_id', $phone_id);
		$this->db->where('password', $password);
		$this->db->join('fakultas', 'member.id_fakultas = fakultas.id_fakultas');
		$this->db->join('user', 'user.id_member = member.id_member');
		$this->db->join('premium_member', 'premium_member.id_user = user.id_user','left');
		$datauser = $this->db->get('member')->row();
		if (!empty($datauser)) {
			if ($datauser->phone_id==null) {
					$this->db->where('fcm_token', $token);
					$this->db->update('user', array('fcm_token' => ""));

					$this->db->where('phone_id', $phone_id);
					$this->db->update('user', array('phone_id' => NULL));

					$this->db->where('id_user', $datauser->id_user);
					$this->db->update('user', array('fcm_token' => $token ,'phone_id'=> $phone_id));
					$output['status'] = 'success';
					$output['result'] = $datauser;
			}else{
				if ($phone_id!=$datauser->phone_id) {
					$output['status'] = 'devicenotsame';
				}else{
					$this->db->where('fcm_token', $token);
					$this->db->update('user', array('fcm_token' => ""));

					$this->db->where('phone_id', $phone_id);
					$this->db->update('user', array('phone_id' => NULL));
				
					$this->db->where('id_user', $datauser->id_user);
					$this->db->update('user', array('fcm_token' => $token ,'phone_id'=> $phone_id));
					$output['status'] = 'success';
					$output['result'] = $datauser;
				}
			}
		}else{
			$output['status'] = 'fail';
		}

		return $output;
	}

	function getPoinById($id)
	{
		$this->db->where('id_pengundang', $id);
		return $this->db->get('kodeunik')->result();
	}

	function updateData($data,$jenis,$id_user)
	{
		//ceck email
		if (!empty($data['email'])) {
			if (!$this->checkEmail($data['email'])) {
				$output['status']='emailexist';
				return $output;
			}
		}

		//update data
		$this->db->select('id_member');
		$this->db->where('id_user', $id_user);
		$getId = $this->db->get('user')->row();
		if (!empty($getId)) {
			$this->db->where('id_member', $getId->id_member);
			$this->db->update('member', $data);
			$output['status']='success';
		}else{
			$output['status']='fail';
		}

		return $output;
	}

	function simpanFoto($user_img,$id_user)
	{
		$this->db->select('id_member');
		$this->db->where('id_user', $id_user);
		$user = $this->db->get('user')->row();
		if (empty($user)) {
			$output['status'] = "usernoexist";
		}else{
		    $this->db->where('id_member', $user->id_member);
			$update = $this->db->update('member', array('foto' => $user_img));
			if ($update) {
				$output['status'] = "success";
				$this->db->select('foto');
				$this->db->where('id_member', $user->id_member);
				$output['foto'] = $this->db->get('member')->row();
			}else{
				$output['status'] = "fail";
			}
		}
		return $output;
	}

	function checkEmail($email)
	{
		$this->db->select('user.id_user');
		$this->db->where('member.email', $email);
		$this->db->or_where('kontributor.email', $email);
		$this->db->join('kontributor', 'kontributor.id_kontributor = user.id_kontributor','left');
		$this->db->join('member', 'member.id_member = user.id_member','left');
		$check = $this->db->get('user')->row();
		if (empty($check)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function cekVoucher($kode)
	{
		//check Kode unik
		$this->db->select('kode_unik,id_user');
		$this->db->where('kode_unik', strtoupper($kode));
		$checkuser =$this->db->get('user')->row();
		if (empty($checkuser)) {
			$output['status']="kodenoexist";
		}else{
			$this->db->select('diskon');
			$this->db->where('kode_unik', 'KODE-PREMIUM');
			$poin = $this->db->get('voucher')->row();
			$output['potongan'] = $poin->diskon;
			$output['status']="success";
			$output['kodeunik']=$checkuser->kode_unik;
		}
		//next check voucher
		return $output;
	}

	function logout($id_user)
	{
		$this->db->where('id_user', $id_user);
		return $this->db->update('user', array('fcm_token' => "" ,'phone_id'=> NULL));

	}



}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */