<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function getDataMember()
	{
		$this->db->select("*");
		$this->db->join('fakultas', 'fakultas.id_fakultas = member.id_fakultas');
		$this->db->join('jurusan', 'jurusan.id_jurusan = member.id_jurusan');
		$query = $this->db->get('member');
		return $query->result_array();
	}

	public function getDataKontributor()
	{
		$this->db->join('jurusan', 'jurusan.id_jurusan = kontributor.id_jurusan');
		$query = $this->db->get('kontributor');
		return $query->result_array();
	}

	public function getDataMemberById($id)
	{
		$this->db->where('id_member', $id);
		$query = $this->db->get('member');
		return $query->result_array();
	}

	public function getDataKontributorById($id)
	{
		$this->db->where('id_kontributor', $id);
		$query = $this->db->get('kontributor');
		return $query->result_array();
	}

	public function getDataUniv()
	{
		$query = $this->db->get('universitas');
		return $query->result_array();
	}

	public function getDataFakultas()
	{
		$query = $this->db->get('fakultas');
		return $query->result_array();
	}

	public function getDataJurusan()
	{
		$query = $this->db->get('jurusan');
		return $query->result_array();
	}

	public function getDataPaket()
	{
		$query = $this->db->get('paket');
		return $query->result_array();
	}

	public function getLastMember()
	{
		$this->db->group_by('id_member', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('member');
		return $query->result_array();
	}

	public function getLastKontributor()
	{
		$this->db->group_by('id_kontributor', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('kontributor');
		return $query->result_array();
	}

	public function addMember()
	{
		// $str = $this->input->post('nama').$this->input->post('password');
		// $random = str_shuffle($str);
		// 'kode_unik' => strtoupper(substr($random,0,8))
		$object = array('nama' => $this->input->post('nama'),
						'notelp' => $this->input->post('notelp'),
						'id_fakultas' => $this->input->post('fakultas'),
						'id_jurusan' => $this->input->post('jurusan'),
						'email' => $this->input->post('email'),
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password'));
		$this->db->insert("member", $object);
	}

	public function insertUserMember($id)
	{
		$object = array('id_member' => $id,
						'id_kontributor' => '');
		$this->db->insert("user", $object);
	}

	public function addKontributor()
	{
		// $str = $this->input->post('nama').$this->input->post('password');
		// $random = str_shuffle($str);
		// 'kode_unik' => strtoupper(substr($random,0,8))
		$object = array('nama' => $this->input->post('nama'),
						'id_jurusan' => $this->input->post('mapel'),
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password'));
		$this->db->insert("kontributor", $object);
	}

	public function insertUserKontributor($id)
	{
		$object = array('id_member' => '',
						'id_kontributor' => $id);
		$this->db->insert("user", $object);
	}

	public function updateMember($id)
	{
		$object = array('nama' => $this->input->post('nama'),
						'notelp' => $this->input->post('notelp'),
						'id_fakultas' => $this->input->post('fakultas'),
						'id_jurusan' => $this->input->post('jurusan'),
						'email' => $this->input->post('email'),
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password'));
		$this->db->where('id_member', $id);
		$this->db->update('member', $object);
	}

	public function updateKontributor($id)
	{
		$object = array('nama' => $this->input->post('nama'),
						'mapel' => $this->input->post('mapel'),
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password'));
		$this->db->where('id_kontributor', $id);
		$this->db->update('kontributor', $object);
	}

	public function deleteMember($id)
	{
		$this->db->where('id_member', $id);
		$this->db->delete("member");
	}

	public function deleteKontributor($id)
	{
		$this->db->where('id_kontributor', $id);
		$this->db->delete("kontributor");
	}
}
?>