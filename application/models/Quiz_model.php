<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz_model extends CI_Model {

	function mengambilSoalTeori($total)
	{
		//mengambil data teori
		$this->db->order_by('RAND ()');
		$result['teori'] = $this->db->get('tb_bank_soal_teori',$total)->result_array();
		if (!empty($result['teori'])) {
			for ($i = 0; $i < count($result['teori']); $i++) {
				$this->db->where('bank_soal_id', $result['teori'][$i]['id_bank_soal']);
				$result['teori'][$i]['jawaban'] = $this->db->get('tb_bank_jawaban')->result();
			}
			$result['status'] = true;
		}else{
			$result['status'] = false;
			$result['messgae'] = "tidak dapat mengambil soal";
		}
		return $result;
	}

	function mengambilSoalPraktikum($total)
	{
		
		$this->db->order_by('RAND ()');
		$result['praktikum'] = $this->db->get('tb_bank_soal_praktikum',$total)->result_array();
		if (!empty($result['praktikum'])) {
			$result['status'] = true;
		}else{
			$result['status'] = false;
			$result['messgae'] = "tidak dapat mengambil soal";
		}
		return $result;
	}

}

/* End of file Quiz_model.php */
/* Location: .//C/Users/andri/AppData/Local/Temp/fz3temp-2/Quiz_model.php */