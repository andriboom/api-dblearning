<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Quiz extends REST_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('quiz_model','quiz');
	}

	public function index_get()
	{
		$this->response([
				'status' => FALSE,
				'message' => 'opps something error'
			], REST_Controller::HTTP_OK);
	}


	function teori_post()
	{
		$totalsoal = $this->post('total');		
		if (empty($totalsoal)) {
			$this->response([
				'status' => FALSE,
				'message' => 'parameter tidak valid'
			], REST_Controller::HTTP_OK);
		}else{
			$this->response($this->quiz->mengambilSoalTeori($totalsoal), REST_Controller::HTTP_OK);
		}
	}

	function praktikum_post()
	{
		$totalsoal = $this->post('total');		
		if (empty($totalsoal)) {
			$this->response([
				'status' => FALSE,
				'message' => 'parameter tidak valid'
			], REST_Controller::HTTP_OK);
		}else{
			$this->response($this->quiz->mengambilSoalPraktikum($totalsoal), REST_Controller::HTTP_OK);
		}
	}

}

/* End of file Quiz.php */
/* Location: .//C/Users/andri/AppData/Local/Temp/fz3temp-2/Quiz.php */