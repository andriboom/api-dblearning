<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class Materi extends REST_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('materi_model','materi');
	}

	public function index_get()
	{
		$this->response($this->materi->getAll(), REST_Controller::HTTP_OK);
	}

}

/* End of file Materi.php */
/* Location: .//C/Users/andri/AppData/Local/Temp/fz3temp-2/Materi.php */