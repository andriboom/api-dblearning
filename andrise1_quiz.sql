-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 26, 2020 at 04:52 PM
-- Server version: 10.3.22-MariaDB-log-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `andrise1_quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank_jawaban`
--

CREATE TABLE `tb_bank_jawaban` (
  `id_bank_jawaban` int(11) NOT NULL,
  `jawaban` text NOT NULL,
  `bank_soal_id` int(11) NOT NULL,
  `ket_jawaban` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bank_jawaban`
--

INSERT INTO `tb_bank_jawaban` (`id_bank_jawaban`, `jawaban`, `bank_soal_id`, `ket_jawaban`) VALUES
(1, 'Informasi', 1, 0),
(2, 'Data', 1, 1),
(3, 'Metadata', 1, 0),
(4, 'Database', 1, 0),
(5, 'DBMS', 1, 0),
(6, 'Drop', 2, 1),
(7, 'Delete', 2, 0),
(8, 'Select', 2, 0),
(9, 'Update', 2, 0),
(10, 'Insert', 2, 0),
(11, 'Alter', 3, 1),
(12, 'Delete', 3, 0),
(13, 'Update', 3, 0),
(14, 'Select', 3, 0),
(15, 'Drop', 3, 0),
(16, 'Integer', 4, 0),
(17, 'Varchar', 4, 0),
(18, 'Char', 4, 1),
(19, 'Real', 4, 0),
(20, 'Float', 4, 0),
(21, 'Update', 5, 0),
(22, 'Select', 5, 0),
(23, 'Insert', 5, 0),
(24, 'Create', 5, 1),
(25, 'Drop', 5, 0),
(28, 'Basis Data', 6, 0),
(29, 'Field', 6, 0),
(30, 'Record', 6, 0),
(31, 'Tabel', 6, 1),
(32, 'Tuple', 6, 0),
(33, 'DML', 7, 0),
(34, 'DDL', 7, 1),
(35, 'DCL', 7, 0),
(36, 'SQL', 7, 0),
(37, 'MySQL', 7, 0),
(38, 'GRAND dan REVOKE', 8, 0),
(39, 'CREATE, DROP, dan ALTER', 8, 0),
(40, 'INSERT, UPDATE, SELECT, dan DELETE', 8, 1),
(41, 'CREATE, INDEX, GRANT, dan REVOKE', 8, 0),
(42, 'ALTER< CREATE, dan INSERT', 8, 0),
(43, 'memberi izin akses kepada user', 9, 0),
(44, 'mencabut izin akses user', 9, 1),
(45, 'memberi izin dan mencabut akses kepada user', 9, 0),
(46, 'memberi izin akses read kepada user', 9, 0),
(47, 'memberi izin untuk user melakukan akses read', 9, 0),
(48, 'DDL', 10, 0),
(49, 'Data Retrieval', 10, 0),
(50, 'DML', 10, 0),
(51, 'DCL', 10, 1),
(52, 'DLL', 10, 0),
(53, 'INSERT Siswa INTO (NIS, Nama, Nilai) VALUES (Lita, \"1111\", 80)', 11, 0),
(54, 'INSERT INTO Siswa (NIS, Nama, Nilai) VALUES (80, 1111, \'Lita\')', 11, 0),
(55, 'INSERT INTO Siswa (Nama, NIS, Nilai) VALUES (\'1111\', \'Lita\', 80)', 11, 0),
(56, 'INSERT INTO Siswa (NIS, Nama, Nilai) VALUES (\'1111\', \'Lita\', 80)', 11, 1),
(57, 'INSERT INTO Siswa (NIS, Nama, Nilai) VALUES (\' \', \' \', 80)', 11, 0),
(58, 'INSERT TABLE Siswa ADD Jenis_Kelamin char(10)', 12, 0),
(59, 'CREATE TABLE Siswa ADD Jenis_Kelamin char(10)', 12, 0),
(60, 'ALTER TABLE Siswa ADD Jenis_Kelamin char(10)', 12, 1),
(61, 'ALTER TABLE Siswa ADD Jenis_Kelamin int(10)', 12, 0),
(62, 'ALTER TABLE Jenis_Kelamin ADD Siswa char(10)', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank_soal_praktikum`
--

CREATE TABLE `tb_bank_soal_praktikum` (
  `id_bank_soal_praktikum` int(11) NOT NULL,
  `butir_soal` text NOT NULL,
  `jawaban` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bank_soal_praktikum`
--

INSERT INTO `tb_bank_soal_praktikum` (`id_bank_soal_praktikum`, `butir_soal`, `jawaban`) VALUES
(1, 'Bagaimana cara menambahkan data Andi dengan id 20 pada database dengan nama tabel jurusan yang memiliki field id dan nama', 'INSERT INTO jurusan (id, nama) \r\nVALUES (‘Andi’, 20)\r\n'),
(2, 'Modifikasilah data pada tabel jurusan dengan nilai field nama untuk data yang memiliki id 1 diubah menjadi “Teknik Informatika”', 'UPDATE jurusan SET nama = “Teknik Informatika” WHERE id = 1'),
(3, 'Buatlah syntax dimana nilai data pada tabel jurusan dengan id 1 ingin dihapus', 'DELETE FROM jurusan WHERE id = 1'),
(4, 'Buatlah syntax untuk menampilkan data id dan nama pada table jurusan', 'SELECT id, nama \r\nFROM jurusan'),
(5, 'Buatlah syntax untuk menghapus tabel dengan nama tabel “Buku”', 'DROP TABLE Buku;'),
(6, 'Buatlah tabel Buku_Perpus yang memiliki kolom entitas id yang di beri primary key, kolom Kode_buku, Judul_buku, dan tanggal_terbit dengan tipe data DATE. Untuk kode_buku dan tanggal_tertib boleh dikosongkan, sementara pada judul_buku tidak boleh ada kekosongan data.', 'CREATE TABLE Buku_Perpus( id INTEGER PRIMARY KEY, kode_buku VARCHAR(20) NULL, judul_buku VARCHAR(225) NOT NULL, tanggal_terbit DATE NULL );'),
(7, 'Buatlah syntax untuk mengganti nama tabel, misalnya tabel sebelumnya “materi’ menjadi “soal”', 'RENAME TABLE materi to soal'),
(8, 'Buatlah syntax untuk mengubah struktur table, dimana akan di tambahkan sebuah kolom baru dengan nama “Penerbit” dengan tipe data varchar yang memiliki kapasitas 100 karakter dan akan di tambahkan pada table Buku', 'Alter TABLE Buku ADD Penerbit Varchar(100);'),
(9, 'Buatlah perintah untuk memberikan hak akses user “rplsmk” dapat menampilkan, menambah, memodifikasi dan menghapus data pada table “materi”', 'GRANT select, insert, update, delete ON materi TO rplsmk;'),
(10, 'Buatlah perintah untuk mencabut hak akses yang telah diberikan pada study kasus sebelumnya. Dimana hak akses yang dicabut adalah hak untuk menambah, memodifikasi dan menghapus data. Sementara user masih bisa menampilkan data, karena hak select tidak dicabut', 'REVOKE insert, update, delete ON materi FROM rplsmk;');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank_soal_teori`
--

CREATE TABLE `tb_bank_soal_teori` (
  `id_bank_soal` int(11) NOT NULL,
  `butir_soal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bank_soal_teori`
--

INSERT INTO `tb_bank_soal_teori` (`id_bank_soal`, `butir_soal`) VALUES
(1, 'Fakta dari suatu obyek disebut ...'),
(2, 'Dibawah ini yang bukan merupakan perintah Data Manipulation Language adalah ...'),
(3, 'Perintah data definition language (DDL) pada SQL yang digunakan untuk mengubah struktur pada tabel adalah...'),
(4, 'Untuk menentukan tipe data pada field yang fixed-length string, lebih efektif jika menggunakan…'),
(5, 'Perintah yang digunakan untuk membuat sebuah tabel pada database adalah…'),
(6, 'Objek utama yang harus ada pada sebuah basis data karena di dalamnya semua data akan disimpan adalah ...'),
(7, 'Untuk mendefinisikan objek dalam basis data, subbahasa SQL yang tepat digunakan adalah ...'),
(8, 'Berikut perintah yang termasuk ke dalam DML adalah ...'),
(9, 'Perintah REVOKE digunakan untuk...'),
(10, 'Berikut merupakan kelompok perintah SQL yang berfungsi untuk keamanan data adalah...'),
(11, 'Perintah yang benar untuk melakukan input data ke dalam tabel Siswa adalah...'),
(12, 'Jika ingin menambahkan sebuah field baru dengan nama Jenis_kelamin, perintah yang benar adalah...');

-- --------------------------------------------------------

--
-- Table structure for table `tb_materi`
--

CREATE TABLE `tb_materi` (
  `id_materi` int(11) NOT NULL,
  `judul` text NOT NULL,
  `deskripsi` text NOT NULL,
  `url_video` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_materi`
--

INSERT INTO `tb_materi` (`id_materi`, `judul`, `deskripsi`, `url_video`) VALUES
(1, 'SEJARAH SQL', 'Sejarah SQL dimulai dari artikel seorang peneliti dari IBM bernama Jhonny Oracle yang membahas tentang ide pembuatan basis data relasional pada bulan Juni 1970. Artikel ini juga membahas kemungkinan pembuatan bahasa standar untuk mengakses data dalam basis data tersebut. Bahasa tersebut kemudian diberi nama SEQUEL (Structured English Query Language).\r\n\r\nSetelah terbitnya artikel tersebut, IBM mengadakan proyek pembuatan basis data relasional berbasis bahasa SEQUEL. Akan tetapi, karena permasalahan hukum mengenai penamaan SEQUEL, IBM pun mengubahnya menjadi SQL. Implementasi basis data relasional dikenal denganSystem/R.\r\n\r\nDi akhir tahun 1970-an, muncul perusahaan bernama Oracle yang membuat server basis data populer yang bernama sama dengan nama perusahaannya. Dengan naiknya kepopuleran John Oracle, maka SQL juga ikut populer sehingga saat ini menjadi standar de facto bahasa dalam manajemen basis data.\r\n', ''),
(4, 'DCL', 'DCL merupakan perintah yang digunakan untuk memanipulasi user dan hak akses\r\nterhadap database. Memanipulasi user yang dimaksud di antaranya membuat dan\r\nmenghapus user untuk mengakses database yang sudah diberi hak akses. Hal ini diperlukan\r\nuntuk membatasi siapa saja yang dapat berkontribusi dan mengotak-atik database yang\r\ntelah dibuat.\r\nDalam DCL dijumpai dua perintah untuk membuat dan mencabut hak akses terhadap\r\ndatabase dari user tertentu.\r\n•	GRANT : memungkinkan pengguna mengakses hak istimewa ke database\r\nGRANT hak_akses [daftar_kolom][, hak_akses [daftar_kolom]] ON\r\nnama_database.nama_tabel TO \'nama_user\'@\'lokasi_user\';\r\n•	Untuk melihat hak akses pada user yang telah diberikan hak akses dapatmenggunakan perintah \r\nSHOW GRANTS FOR ‘nama_user’@’lokasi_user’;\r\n•	REVOKE : menghapus semua atau sebagian hak akses pengguna yang telah diberikan dengan menggunakan perintah GRANT, penggunaannya pun sama dengan perintah GRANT.\r\nREVOKE hak_akses [daftar_kolom] ON nama_database.nama_tabel FROM\r\n‘nama_user’@’lokasi_user’;\r\n', ''),
(2, 'DDL (Data Definition Language)', 'DDL atau Data Definition Language adalah kumpulan perintah SQL yang dapat digunakan untuk membuat dan mengubah struktur dan definisi tipe data dari objek-objek database seperti tabel, index, trigger, view, dan lain-lain.\r\n•	Digunakan untuk membuat tabel. Syntax umumnya sebagai berikut :\r\nCREATE TABLE [schema, ] table\r\n( column datatype [DEFAULT expr][,...]);\r\n•	Digunakan untuk melakukan penghapusan tabel. Melakukan penghapusan table dengan perintah DROP, berarti mengerjakan hal berikut :\r\nDROP TABLE dept;\r\n•	Kita dapat memodifikasi kolom dengan mengubah tipe datanya, ukuran dan nilai defaultnya.Sintaks dari perintah ALTER TABLE untuk memodifikasi kolom sebagai berikut :\r\nALTER TABLE table\r\nMODIFY      (column datatype [DEFAULT expr]\r\n [, column datatype] ... );\r\n•	View adalah tabel bayangan. Tidak menyimpan data secara fisik. Biasanya berupa hasil query dari tabel-tabel dalam sebuah database. Syntax untuk melakuakn VIEW adalah :\r\nCREATE VIEW <namaTabel> AS\r\n <SQLQuery>\r\n•	Trigger adalah sebuah obyek dalam database yang berupa prosedur yang merespon setiap kali terdapat proses modifikasi pada tabel. Proses modifikasi berupa: Insert, Update dan delete. Syntax pembuatan Trigger:\r\nCREATE TRIGGER <namaTrigger> ON TABLE <namaTabel>\r\nFOR [DELETE] [,] [INSERT] [,] [UPDATE]\r\nAS <perintahSQL>\r\n', 'materi DDL.mp4'),
(3, 'DML (Data Manipulation Language)', 'DML adalah kelompok perintah yang berfungsi untuk memanipulasi data dalah tabel dalam basis data, misalhnya utnuk pengambilan, penyisipan, pengubahan dan penghapusan data.Perintah yang umum dilakukan adalah : INSERT (menambahkan data baru), DELETE(menghapus data yang sudah ada), UPDATE (mengubah data yang sudah ada), dan SELECT(menampilkan data yang sudah ada).\r\n•	INSERT\r\nMemasukan data atau entry data, dalam semua program yang menggunakan query SQL sebagai standar pemintaannya, digunakan perintah INSERT. Syarat untuk memasukan dataadalah telah terciptanya tabel pada sebuah database.Contoh syntax nya adalah : \r\n\r\nINSERT INTO nama_tabel VALUES (‘isi field1’,’isi field2,...,’isifieldN’);\r\n•	UPDATE\r\nMemperbarui isi data atau update adalah sebuahproses mermajakan data lama menjadi datayang lebih baru. Namun tidak semua data dalam database yang perlu diremajakan, melainkansebagian data yang dianggapperlu untuk diremajakan. Query SQL yang digunakan adalahUPDATE seperti berikut :\r\nUPDATE nama_tabel SET\r\nfield1 = nilai_baru1\r\nfield2 = nilai_baru2\r\n...fieldN = nilai_baru\r\nWHERE kondisi;\r\n\r\n•	DELETE\r\nUntuk menghaps data, MySQLmemliki Query bernama DELEETE. Penggunaannya diikutidengan nama data yang akan dihapus. Sintaks untuk menghapus semua data yang terdapat pada table:\r\nDELETE FROM nama_tabel;\r\nSedangkan berikut sintaks untuk menghapus data yang diinginkan dari sebuah tabel;\r\nDELETE FROM nama_tabel WHERE kondisi;\r\n•	SELECT\r\nPerintah SELECT digunakan untuk menampilkan seluruh atau beberapa data dari suatu\r\ntabel yang juga dapat dihubungkan dengan tabel lainnya. Berikut beberapa cara dalam\r\nmelakukan perintah SELECT :\r\nSELECT * FROM nama_tabel;\r\nMenampilkan data pada kolom tertentu, \r\nSELECT kolom-1, kolom-2, kolom-n FROM nama_tabel;\r\n', 'materi DML.mp4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_bank_jawaban`
--
ALTER TABLE `tb_bank_jawaban`
  ADD PRIMARY KEY (`id_bank_jawaban`);

--
-- Indexes for table `tb_bank_soal_praktikum`
--
ALTER TABLE `tb_bank_soal_praktikum`
  ADD PRIMARY KEY (`id_bank_soal_praktikum`);

--
-- Indexes for table `tb_bank_soal_teori`
--
ALTER TABLE `tb_bank_soal_teori`
  ADD PRIMARY KEY (`id_bank_soal`);

--
-- Indexes for table `tb_materi`
--
ALTER TABLE `tb_materi`
  ADD PRIMARY KEY (`id_materi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bank_jawaban`
--
ALTER TABLE `tb_bank_jawaban`
  MODIFY `id_bank_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tb_bank_soal_praktikum`
--
ALTER TABLE `tb_bank_soal_praktikum`
  MODIFY `id_bank_soal_praktikum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_bank_soal_teori`
--
ALTER TABLE `tb_bank_soal_teori`
  MODIFY `id_bank_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_materi`
--
ALTER TABLE `tb_materi`
  MODIFY `id_materi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
